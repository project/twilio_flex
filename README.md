# Twilio Flex WebChat

Flex WebChat is a chat widget that you can embed on your website. 
The widget helps your customers chat with an agent without needing 
to leave your webpage. It is natively integrated with the Flex Contact Center.

https://www.twilio.com/docs/flex/developer/messaging/webchat/setup
