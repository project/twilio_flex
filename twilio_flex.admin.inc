<?php
/**
 * @file
 * Administrative page callbacks for the twilio_flex module.
 */

/**
 * Twilio Flex settings form.
 */
function twilio_flex_admin_settings_form($form_state) {
  $form = [];

  $settings = twilio_flex_get_settings();

  $form['twilio_flex_account_sid'] = [
    '#parents' => ['twilio_flex_settings', 'twilio_flex_account_sid'],
    '#title' => t('Account SID'),
    '#type' => 'textfield',
    '#default_value' => $settings['twilio_flex_account_sid'],
    '#size' => 40,
    '#maxlength' => 40,
      '#required' => TRUE,
      '#description' => t('Twilio Account SID, ie AC...'),
  ];

  $form['twilio_flex_flow_sid'] = [
    '#parents' => ['twilio_flex_settings', 'twilio_flex_flow_sid'],
    '#title' => t('Flex Flow SID'),
    '#type' => 'textfield',
    '#default_value' => $settings['twilio_flex_flow_sid'],
    '#size' => 40,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => t('Twilio Flex Flow SID, ie FO...'),
  ];

  $form['twilio_flex_main_header_title'] = [
    '#parents' => ['twilio_flex_settings', 'twilio_flex_main_header_title'],
    '#title' => t('Header title'),
    '#type' => 'textfield',
    '#default_value' => $settings['twilio_flex_main_header_title'],
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Header title (default: <em>Chat with Us</em>)'),
  ];

  $form['twilio_flex_msg_canvas_welcome'] = [
    '#parents' => ['twilio_flex_settings', 'twilio_flex_msg_canvas_welcome'],
    '#title' => t('Canvas welcome message'),
    '#type' => 'textfield',
    '#default_value' => $settings['twilio_flex_msg_canvas_welcome'],
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Canvas welcome message (default: <em>Welcome to customer service</em>)'),
  ];

  $form['twilio_flex_msg_canvas_predefined_author'] = [
    '#parents' => ['twilio_flex_settings', 'twilio_flex_msg_canvas_predefined_author'],
    '#title' => t('First message author'),
    '#type' => 'textfield',
    '#default_value' => $settings['twilio_flex_msg_canvas_predefined_author'],
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('First message author (default: <em>Bot</em>)'),
  ];

  $form['twilio_flex_msg_canvas_predefined_body'] = [
    '#parents' => ['twilio_flex_settings', 'twilio_flex_msg_canvas_predefined_body'],
    '#title' => t('First message body'),
    '#type' => 'textfield',
    '#default_value' => $settings['twilio_flex_msg_canvas_predefined_body'],
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('First message body (default: <em>Hi there! How can we help you today?</em>)'),
  ];

  $form['twilio_flex_on'] = [
    '#parents' => ['twilio_flex_settings', 'twilio_flex_on'],
    '#title' => t('Chat enabled'),
    '#type' => 'checkbox',
    '#default_value' => $settings['twilio_flex_on'],
  ];

  return system_settings_form($form);
}
